﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Recipes.API.Installers.Interfaces;
using Recipes.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recipes.API.Installers
{
    public class DbInstaller: IInstaller
    {
        public void InstallServices(IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDbContext<RecipesContext>(options =>
                options.UseSqlServer(configuration["ConnectionString:RecipesAppDB"], b => b.MigrationsAssembly("Recipes.API")));
        }
    }
}
