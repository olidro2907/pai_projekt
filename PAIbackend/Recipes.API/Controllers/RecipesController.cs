﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Requests.Recipes.DeleteRecipe;
using Recipes.Infrastructure.Requests.Recipes.GetRecipe;
using Recipes.Infrastructure.Requests.Recipes.GetRecipes;
using Recipes.Infrastructure.Requests.Recipes.PostRecipe;
using Recipes.Infrastructure.Requests.Recipes.UpdateRecipe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recipes.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecipesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RecipesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<Response<IEnumerable<RecipeDTO>>> GetRecipes()
            => await _mediator.Send(new GetRecipesRequest());

        [HttpGet("{recipeId}")]
        public async Task<Response<RecipeDTO>> GetRecipe(Guid recipeId)
            => await _mediator.Send(new GetRecipeRequest(recipeId));

        [HttpPost]
        public async Task<Response<RecipeDTO>> PostRecipe(PostRecipeRequest request)
            => await _mediator.Send(request);

        [HttpDelete("{recipeId}")]
        public async Task<Response<RecipeDTO>> DeleteRecipe(Guid recipeId)
             => await _mediator.Send(new DeleteRecipeRequest(recipeId));

        [HttpPut]
        public async Task<Response<RecipeDTO>> UpdateRecipe(UpdateRecipeRequest request)
            => await _mediator.Send(request);
    }
}
