﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Requests.Ingredients.DeleteIngredient;
using Recipes.Infrastructure.Requests.Ingredients.GetIngredient;
using Recipes.Infrastructure.Requests.Ingredients.PostIngredient;
using Recipes.Infrastructure.Requests.Ingredients.UpdateIngredient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recipes.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IngredientsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public IngredientsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("{id}")]
        public async Task<Response<IngredientDTO>> GetIngredient(Guid id)
             => await _mediator.Send(new GetIngredientRequest(id));

        [HttpPost]
        public async Task<Response<IngredientDTO>> PostIngredient(PostIngredientRequest request)
            => await _mediator.Send(request);


        [HttpDelete("{id}")]
        public async Task<Response<IngredientDTO>> DeleteIngredient(Guid id)
             => await _mediator.Send(new DeleteIngredientRequest(id));

        [HttpPut]
        public async Task<Response<IngredientDTO>> UpdateIngredient(UpdateIngredientRequest request)
             => await _mediator.Send(request);

    }
}
