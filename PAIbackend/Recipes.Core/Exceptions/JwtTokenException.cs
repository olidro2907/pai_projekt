﻿using System;

namespace Recipes.Core.Exceptions
{
    public class JwtTokenException: Exception
    {
        public JwtTokenException(string message) : base(message)
        {
        }
    }
}
