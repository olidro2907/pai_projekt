﻿using System;

namespace Recipes.Core.Exceptions
{
    public class BadRequestException: Exception
    {
        public BadRequestException()
        {
        }
    }
}
