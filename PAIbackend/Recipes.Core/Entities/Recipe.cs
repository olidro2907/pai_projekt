﻿using Recipes.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Core.Entities
{
    public class Recipe
    {
        public Guid Id { get; set; }

        public string UserId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        //public string ImageUrl { get; set; }

        public RecipeType Type { get; set; }

        public DateTime Created { get; set; }

        //public int? IngredientsOnPage { get; set; }
        public virtual User User { get; set; }


        public virtual ICollection<Ingredient> Ingredients { get; set; }
    }
}
