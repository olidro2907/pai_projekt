﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Core.Entities
{
    public class Ingredient
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Amount { get; set; }

        public Guid RecipeId { get; set; }

        public DateTime Created { get; set; }

        public virtual Recipe Recipe{ get; set; }

    }
}
