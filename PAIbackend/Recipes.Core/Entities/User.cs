﻿using Microsoft.AspNetCore.Identity;

namespace Recipes.Core.Entities
{
    public class User: IdentityUser
    {
        public string Name { get; set; }

        public string Surname { get; set; }
    }
}
