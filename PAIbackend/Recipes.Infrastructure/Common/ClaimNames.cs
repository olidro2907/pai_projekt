﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Common
{
    public static class ClaimNames
    {
        public const string Id = "id";

        public const string Role = "role";
    }
}
