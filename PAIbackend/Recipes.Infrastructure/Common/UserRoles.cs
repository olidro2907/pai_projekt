﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Common
{
    public static class UserRoles
    {
        public const string Administrator = "Administrator";
        public const string User = "User";
    }
}
