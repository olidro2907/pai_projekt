﻿using Microsoft.EntityFrameworkCore;
using Recipes.Core.Entities;
using Recipes.Infrastructure.Context;
using Recipes.Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Repositories
{
    public class RecipesRepository : RepositoryBase<Recipe>, IRecipesRepository
    {
        private readonly RecipesContext _context;

        public RecipesRepository(RecipesContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Recipe> GetByIdWithIngredientsAsync(Guid id)
            => await _context.Set<Recipe>()
                .Include(x => x.Ingredients)
                .FirstOrDefaultAsync(x => x.Id == id);
    }
}
