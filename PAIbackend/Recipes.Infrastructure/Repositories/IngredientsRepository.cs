﻿using Microsoft.EntityFrameworkCore;
using Recipes.Core.Entities;
using Recipes.Infrastructure.Context;
using Recipes.Infrastructure.Repositories.Interfaces;
using System;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Repositories
{
    public class IngredientsRepository : RepositoryBase<Ingredient>, IIngredientsRepository
    {
        private readonly RecipesContext _context;

        public IngredientsRepository(RecipesContext context) : base(context)
        {
            _context = context;
        }

        public Task<Ingredient> GetById(Guid id)
            => _context.Set<Ingredient>()
                .FirstOrDefaultAsync(x => x.Id == id);
    }
}
