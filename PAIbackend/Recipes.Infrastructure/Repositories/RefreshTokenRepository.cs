﻿using Microsoft.EntityFrameworkCore;
using Recipes.Core.Entities;
using Recipes.Infrastructure.Context;
using Recipes.Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Repositories
{
    public class RefreshTokenRepository : RepositoryBase<RefreshToken>, IRefreshTokenRepository
    {
        private readonly RecipesContext _context;

        public RefreshTokenRepository(RecipesContext context) : base(context)
        {
            _context = context;
        }
        public async Task<IEnumerable<RefreshToken>> GetUserRefreshTokensAsync(string userId)
        {
            return await _context.RefreshTokens
                    .Where(x => x.UserId == userId && !x.Used &&
                    !x.Invalidated && x.ExpirationDate > DateTime.UtcNow)
                    .ToListAsync();
        }
    }
}
