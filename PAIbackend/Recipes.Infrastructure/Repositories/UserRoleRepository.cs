﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Recipes.Infrastructure.Context;
using Recipes.Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Repositories
{
    public class UserRoleRepository: IUserRoleRepository
    {
        private readonly RecipesContext _context;

        public UserRoleRepository(RecipesContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<IdentityRole>> GetAllRolesAsync()
            => await _context.Roles.ToListAsync();

        public async Task<IdentityRole> GetRoleByNameAsync(string name)
            => await _context.Roles.SingleOrDefaultAsync(r => r.Name == name);

    }
}
