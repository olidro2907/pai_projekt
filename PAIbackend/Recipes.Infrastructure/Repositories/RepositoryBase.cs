﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Recipes.Infrastructure.Context;
using Recipes.Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private readonly RecipesContext _context;

        public RepositoryBase(RecipesContext context)
        {
            _context = context;
        }

        public T GetById(Guid id)
            => _context.Set<T>().Find(id);

        public async Task<T> GetByIdAsync(Guid id)
            => await _context.Set<T>().FindAsync(id);

        public IEnumerable<T> GetAll()
            => _context.Set<T>().AsNoTracking().ToList();

        public async Task<IEnumerable<T>> GetAllAsync()
            => await _context.Set<T>().AsNoTracking().ToListAsync();

        public IEnumerable<T> FindByCondition(Expression<Func<T, bool>> expression)
            => _context.Set<T>().Where(expression).ToList();

        public async Task<IEnumerable<T>> FindByConditionAsync(
            Expression<Func<T, bool>> expression)
            => await _context.Set<T>().Where(expression).ToListAsync();

        public EntityEntry<T> Add(T entity)
            => _context.Set<T>().Add(entity);

        public async Task<EntityEntry<T>> AddAsync(T entity)
            => await _context.Set<T>().AddAsync(entity);

        public void Delete(T entity)
            => _context.Remove(entity);

        public async Task SaveAsync()
            => await _context.SaveChangesAsync();

        public void Update(T entity)
            => _context.Update(entity);
    }
}
