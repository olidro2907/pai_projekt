﻿using Recipes.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Repositories.Interfaces
{
    public interface IRefreshTokenRepository : IRepositoryBase<RefreshToken>
    {
        Task<IEnumerable<RefreshToken>> GetUserRefreshTokensAsync(string userId);
    }
}
