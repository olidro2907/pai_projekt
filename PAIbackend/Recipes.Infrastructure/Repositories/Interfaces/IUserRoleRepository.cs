﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Repositories.Interfaces
{
    public interface IUserRoleRepository
    {
        Task<IEnumerable<IdentityRole>> GetAllRolesAsync();

        Task<IdentityRole> GetRoleByNameAsync(string name);
    }
}
