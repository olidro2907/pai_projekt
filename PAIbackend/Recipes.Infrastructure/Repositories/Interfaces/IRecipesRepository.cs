﻿using Recipes.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Repositories.Interfaces
{
    public interface IRecipesRepository : IRepositoryBase<Recipe>
    {
        Task<Recipe> GetByIdWithIngredientsAsync(Guid id);
    }
}
