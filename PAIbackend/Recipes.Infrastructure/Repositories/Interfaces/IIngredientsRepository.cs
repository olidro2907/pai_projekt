﻿using Recipes.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Repositories.Interfaces
{
    public interface IIngredientsRepository : IRepositoryBase<Ingredient>
    {
        Task<Ingredient> GetById(Guid id);
    }
}
