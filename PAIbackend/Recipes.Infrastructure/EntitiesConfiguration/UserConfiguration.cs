﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Recipes.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.EntitiesConfiguration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Ignore(p => p.AccessFailedCount)
               .Ignore(p => p.LockoutEnabled)
               .Ignore(p => p.LockoutEnd)
               .Ignore(p => p.PhoneNumber)
               .Ignore(p => p.PhoneNumberConfirmed)
               .Ignore(p => p.TwoFactorEnabled);
        }
    }
}
