﻿using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Requests.Ingredients.PostIngredient;
using Recipes.Infrastructure.Requests.Ingredients.UpdateIngredient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Services.Interfaces
{
    public interface IIngredientsService
    {
        Task<Response<IngredientDTO>> GetByIdAsync(Guid id);

        Task<Response<IngredientDTO>> DeleteAsync(Guid id);

        Task<Response<IngredientDTO>> UpdateAsync(UpdateIngredientRequest request);

        Task<Response<IngredientDTO>> PostAsync(PostIngredientRequest request);
    }
}
