﻿using Recipes.Core.Entities;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Services.Interfaces
{
    public interface IJwtTokenService
    {
        Task<AuthResponseDTO> GenerateJwtTokenWithRefreshToken(User user);

        Task<Response<AuthResponseDTO>> RefreshTokenAsync(string token, string refreshToken);

        Task InvalidateUserRefreshTokensAsync(string userId);
    }
}
