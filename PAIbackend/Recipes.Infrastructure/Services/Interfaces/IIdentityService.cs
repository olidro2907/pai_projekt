﻿using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Services.Interfaces
{
    public interface IIdentityService
    {
        Task<Response<StatusResponseDTO>> SignUpAsync(
            string name, string surname, string email, string password);

        Task<Response<AuthResponseDTO>> SignInAsync(
            string email, string password);

        Task<Response<StatusResponseDTO>> ChangePasswordAsync(
            string password, string newPassword);
    }
}
