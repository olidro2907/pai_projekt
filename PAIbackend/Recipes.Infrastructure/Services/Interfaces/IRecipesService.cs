﻿using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Requests.Recipes.PostRecipe;
using Recipes.Infrastructure.Requests.Recipes.UpdateRecipe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Services.Interfaces
{
    public interface IRecipesService
    {
        Task<Response<RecipeDTO>> GetByIdAsync(Guid id);

        Task<Response<IEnumerable<RecipeDTO>>> GetAllAsync();

        Task<Response<RecipeDTO>> DeleteAsync(Guid id);

        Task<Response<RecipeDTO>> UpdateAsync(UpdateRecipeRequest request);

        Task<Response<RecipeDTO>> PostAsync(PostRecipeRequest request);
    }
}
