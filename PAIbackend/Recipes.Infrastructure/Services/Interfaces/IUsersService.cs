﻿using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Requests.Users.UpdateUser;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Services.Interfaces
{
    public interface IUsersService
    {
        Task<Response<IEnumerable<UserDTO>>> GetUsersAsync();

        Task<Response<UserDTO>> DeleteAsync(Guid id);

        Task<Response<UserDTO>> UpdateAsync(UpdateUserRequest request);
    }
}
