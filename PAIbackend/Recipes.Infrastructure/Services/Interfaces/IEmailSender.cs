﻿using System;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Services.Interfaces
{
    public interface IEmailSender
    {
        Task SendInvitationEmailAsync(string emailAddress, DateTime? startDate, DateTime? expirationDate);
    }
}
