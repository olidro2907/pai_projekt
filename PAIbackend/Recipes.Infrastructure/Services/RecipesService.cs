﻿using Microsoft.AspNetCore.Http;
using Recipes.Core.Entities;
using Recipes.Core.Exceptions;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Repositories.Interfaces;
using Recipes.Infrastructure.Requests.Recipes.PostRecipe;
using Recipes.Infrastructure.Requests.Recipes.UpdateRecipe;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Services
{
    public class RecipesService : IRecipesService
    {
        public readonly IUserRepository _userRepository;
        public readonly IRecipesRepository _recipesRepository;
        public readonly IHttpContextAccessor _httpContextAccessor;

        public RecipesService(
           IRecipesRepository recipesRepository,
           IUserRepository userRepository,
           IHttpContextAccessor httpContextAccessor)
        {
            _userRepository = userRepository;
            _recipesRepository = recipesRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<Response<RecipeDTO>> DeleteAsync(Guid id)
        {
            Recipe recipe = await _recipesRepository.GetByIdAsync(id);

            if (recipe == null)
                throw new NotFoundException("Recipe not found.");

            _recipesRepository.Delete(recipe);
            await _recipesRepository.SaveAsync();

            return new Response<RecipeDTO>(MapToRecipeDTO(recipe));
        }

        public async Task<Response<IEnumerable<RecipeDTO>>> GetAllAsync()
        {
            IEnumerable<Recipe> recipes = await _recipesRepository.GetAllAsync();
            IEnumerable<RecipeDTO> recipesDTO = recipes.Select(s => MapToRecipeDTO(s));

            return new Response<IEnumerable<RecipeDTO>>(recipesDTO);
        }

        public async Task<Response<RecipeDTO>> GetByIdAsync(Guid id)
        {
            Recipe recipe = await _recipesRepository.GetByIdWithIngredientsAsync(id);

            if (recipe == null)
                throw new NotFoundException("Recipe not found");

            return new Response<RecipeDTO>(MapToRecipeDTO(recipe));
        }

        public async Task<Response<RecipeDTO>> PostAsync(PostRecipeRequest request)
        {
            Recipe recipe = new Recipe
            {
                Name = request.Name,
                Description = request.Description,
                Type = request.Type,
                //QuestionsOnPage = request.QuestionsOnPage ?? 1
            };

            await _recipesRepository.AddAsync(recipe);
            await _recipesRepository.SaveAsync();

            recipe = await _recipesRepository.GetByIdAsync(recipe.Id);
            return new Response<RecipeDTO>(MapToRecipeDTO(recipe));
        }

        public async Task<Response<RecipeDTO>> UpdateAsync(UpdateRecipeRequest request)
        {
            if (request.Id == Guid.Empty)
                throw new BadRequestException();

            Recipe recipe = await _recipesRepository.GetByIdAsync(request.Id);

            if (recipe == null)
                throw new NotFoundException("Recipe not found");

            recipe.Name = request.Name ?? recipe.Name;
            recipe.Description = request.Description ?? recipe.Description;
            _recipesRepository.Update(recipe);

            await _recipesRepository.SaveAsync();
            return new Response<RecipeDTO>(MapToRecipeDTO(recipe));
        }

        private RecipeDTO MapToRecipeDTO(Recipe recipe)
        {
            return new RecipeDTO
            {
                Id = recipe.Id,
                Description = recipe.Description,
                Name = recipe.Name,
                Type = recipe.Type,
                //QuestionsOnPage = survey.QuestionsOnPage,
                Ingredients = MapToIngredientDTO(recipe.Ingredients)
            };
        }

        private IEnumerable<IngredientDTO> MapToIngredientDTO(ICollection<Ingredient> ingredients)
        {
            if (ingredients == null)
                return null;

            return ingredients.Select(x => new IngredientDTO()
            {
                Name = x.Name,
                Id = x.Id,
                Amount = x.Amount
            });
        }
    }
}
