﻿using Recipes.Core.Entities;
using Recipes.Core.Exceptions;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Repositories.Interfaces;
using Recipes.Infrastructure.Requests.Ingredients.PostIngredient;
using Recipes.Infrastructure.Requests.Ingredients.UpdateIngredient;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Services
{
    public class IngredientsService : IIngredientsService
    {
        private readonly IIngredientsRepository _ingredientsRepository;

        public IngredientsService(IIngredientsRepository ingredientsRepository)
        {
            _ingredientsRepository = ingredientsRepository;
        }
        public async Task<Response<IngredientDTO>> DeleteAsync(Guid id)
        {
            Ingredient ingredient = await _ingredientsRepository.GetByIdAsync(id);

            if (ingredient == null)
                throw new NotFoundException("Ingredient not found.");

            _ingredientsRepository.Delete(ingredient);
            await _ingredientsRepository.SaveAsync();

            return new Response<IngredientDTO>(MapToIngredientDTO(ingredient));
        }

        public async Task<Response<IngredientDTO>> GetByIdAsync(Guid id)
        {
            Ingredient ingredient = await _ingredientsRepository.GetById(id);

            if (ingredient == null)
                throw new NotFoundException("Ingredient not found");

            return new Response<IngredientDTO>(MapToIngredientDTO(ingredient));
        }

        public async Task<Response<IngredientDTO>> PostAsync(PostIngredientRequest request)
        {
           Ingredient ingredient = new Ingredient
            {
               Name = request.Name,
               Amount = request.Amount,
               RecipeId = request.RecipeId
            };

            await _ingredientsRepository.AddAsync(ingredient);
            await _ingredientsRepository.SaveAsync();

            ingredient = await _ingredientsRepository.GetByIdAsync(ingredient.Id);
            return new Response<IngredientDTO>(MapToIngredientDTO(ingredient));
        }

        public async Task<Response<IngredientDTO>> UpdateAsync(UpdateIngredientRequest request)
        {
            if (request.Id == Guid.Empty)
                throw new BadRequestException();

            Ingredient ingredient = await _ingredientsRepository.GetById(request.Id);

            if (ingredient == null)
                throw new NotFoundException("Ingredient not found");

            ingredient.Name = request.Name ?? ingredient.Name;
            ingredient.Amount = request.Amount ?? ingredient.Amount;

            _ingredientsRepository.Update(ingredient);
            await _ingredientsRepository.SaveAsync();

            return new Response<IngredientDTO>(MapToIngredientDTO(ingredient));
        }

        private static IngredientDTO MapToIngredientDTO(Ingredient ingredient)
        {
            return new IngredientDTO
            {
                Id = ingredient.Id,
                Amount = ingredient.Amount,
                Name = ingredient.Name
            };
        }
    }
}
