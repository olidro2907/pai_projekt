﻿using Microsoft.AspNetCore.Identity;
using Recipes.Core.Entities;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recipes.Infrastructure.DataSeeder
{
    public class Seed : ISeed
    {
        private readonly RecipesContext _context;
        private readonly UserManager<User> _userManager;

        public Seed(RecipesContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public void SeedDatabase()
        {
            SeedRoles();
            SeedUsers();
            SeedUserRoles();
        }

        private void SeedRoles()
        {
            if (!_context.Roles.Any())
            {
                List<IdentityRole> roles = new List<IdentityRole>
                {
                    new IdentityRole
                    {
                        Name = UserRoles.Administrator,
                        NormalizedName = UserRoles.Administrator.ToUpper()
                    },
                    new IdentityRole
                    {
                        Name = UserRoles.User,
                        NormalizedName = UserRoles.User.ToUpper()
                    }
                };

                _context.AddRange(roles);
                _context.SaveChanges();
            }
        }

        private void SeedUsers()
        {
            string password = "Password123!";
            string userEmail = "user@gmail.com";
            string administratorEmail = "administrator@gmail.com";

            if (!_context.Users.Any())
            {
                User administrator = new User
                {
                    Name = "Jan",
                    Surname = "Administrator",
                    UserName = administratorEmail,
                    NormalizedUserName = administratorEmail.ToUpper(),
                    Email = administratorEmail,
                    NormalizedEmail = administratorEmail.ToUpper(),
                    SecurityStamp = Guid.NewGuid().ToString("D")
                };

                User user = new User
                {
                    Name = "Adam",
                    Surname = "User",
                    UserName = userEmail,
                    NormalizedUserName = userEmail.ToUpper(),
                    Email = userEmail,
                    NormalizedEmail = userEmail.ToUpper(),
                    SecurityStamp = Guid.NewGuid().ToString("D")
                };

                administrator.PasswordHash = _userManager.PasswordHasher
                    .HashPassword(administrator, password);
                user.PasswordHash = _userManager.PasswordHasher
                    .HashPassword(user, password);

                _context.Users.AddRange(administrator, user);
                _context.SaveChanges();
            }
        }

        private void SeedUserRoles()
        {
            IdentityRole userRole = _context.Roles.First(r => r.Name == UserRoles.User);
            IdentityRole administratorRole = _context.Roles.First(r => r.Name == UserRoles.Administrator);

            User user = _context.Users.First(u => u.Email == "user@gmail.com");
            User administrator = _context.Users.First(u => u.Email == "administrator@gmail.com");

            if (!_context.UserRoles.Any())
            {
                IdentityUserRole<string> userUserRole = new IdentityUserRole<string>()
                {
                    UserId = user.Id,
                    RoleId = userRole.Id
                };

                IdentityUserRole<string> administratorUserRole = new IdentityUserRole<string>()
                {
                    UserId = administrator.Id,
                    RoleId = administratorRole.Id
                };

                _context.AddRange(userUserRole, administratorUserRole);
                _context.SaveChanges();
            }
        }
    }
}
