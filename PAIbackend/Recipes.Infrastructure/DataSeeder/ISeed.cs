﻿namespace Recipes.Infrastructure.DataSeeder
{
    public interface ISeed
    {
        void SeedDatabase();
    }
}
