﻿using Microsoft.AspNetCore.Http;
using Recipes.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Extensions
{
    public static class HttpContextExtensionAccessor
    {
        public static string GetUserId(this IHttpContextAccessor httpContextAccessor)
        {
            return httpContextAccessor.HttpContext.User?.Claims
                .Single(x => x.Type == ClaimNames.Id).Value;
        }
    }
}
