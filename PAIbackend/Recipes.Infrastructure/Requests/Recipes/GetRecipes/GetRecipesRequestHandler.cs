﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Recipes.GetRecipes
{
    class GetRecipesRequestHandler : IRequestHandler<GetRecipesRequest, Response<IEnumerable<RecipeDTO>>>
    {
        private readonly IRecipesService _recipesService;

        public GetRecipesRequestHandler(IRecipesService recipesService)
        {
            _recipesService = recipesService;
        }

        public async Task<Response<IEnumerable<RecipeDTO>>> Handle(
            GetRecipesRequest request, CancellationToken cancellationToken)
            => await _recipesService.GetAllAsync();
    }
}
