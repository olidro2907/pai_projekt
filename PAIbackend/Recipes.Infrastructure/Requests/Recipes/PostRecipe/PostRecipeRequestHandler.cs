﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Recipes.PostRecipe
{
    class PostRecipeRequestHandler : IRequestHandler<PostRecipeRequest, Response<RecipeDTO>>
    {
        private readonly IRecipesService _recipesService;

        public PostRecipeRequestHandler(IRecipesService recipesService)
        {
            _recipesService = recipesService;
        }

        public async Task<Response<RecipeDTO>> Handle(
            PostRecipeRequest request, CancellationToken cancellationToken)
            => await _recipesService.PostAsync(request);
    }
}
