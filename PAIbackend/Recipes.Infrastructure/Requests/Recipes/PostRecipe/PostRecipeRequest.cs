﻿using MediatR;
using Recipes.Core.Enums;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Recipes.PostRecipe
{
    public class PostRecipeRequest : IRequest<Response<RecipeDTO>>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public RecipeType Type { get; set; }

        //public int? QuestionsOnPage { get; set; }

        public string UserId { get; set; }


        public IEnumerable<RecipeDTO> Ingredients { get; set; }
    }
}
