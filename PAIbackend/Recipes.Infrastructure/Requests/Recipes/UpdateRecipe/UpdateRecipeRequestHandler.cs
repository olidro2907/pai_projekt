﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Recipes.UpdateRecipe
{
    class UpdateRecipeRequestHandler : IRequestHandler<UpdateRecipeRequest, Response<RecipeDTO>>
    {
        private readonly IRecipesService _recipesService;

        public UpdateRecipeRequestHandler(IRecipesService recipesService)
        {
            _recipesService = recipesService;
        }

        public async Task<Response<RecipeDTO>> Handle(
            UpdateRecipeRequest request, CancellationToken cancellationToken)
            => await _recipesService.UpdateAsync(request);
    }
}
