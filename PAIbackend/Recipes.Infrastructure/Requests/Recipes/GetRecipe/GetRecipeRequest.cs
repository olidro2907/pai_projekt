﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Recipes.GetRecipe
{
    public class GetRecipeRequest : IRequest<Response<RecipeDTO>>
    {
        public GetRecipeRequest(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
