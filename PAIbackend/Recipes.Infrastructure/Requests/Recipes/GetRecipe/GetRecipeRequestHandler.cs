﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Recipes.GetRecipe
{
    public class GetRecipeRequestHandler : IRequestHandler<GetRecipeRequest, Response<RecipeDTO>>
    {
        private readonly IRecipesService _recipesService;

        public GetRecipeRequestHandler(IRecipesService recipesService)
        {
            _recipesService = recipesService;
        }

        public async Task<Response<RecipeDTO>> Handle(
            GetRecipeRequest request, CancellationToken cancellationToken)
            => await _recipesService.GetByIdAsync(request.Id);
    }
}
