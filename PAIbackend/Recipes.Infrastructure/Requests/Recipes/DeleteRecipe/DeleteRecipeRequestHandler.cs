﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Recipes.DeleteRecipe
{
    class DeleteRecipeRequestHandler : IRequestHandler<DeleteRecipeRequest, Response<RecipeDTO>>
    {
        private readonly IRecipesService _recipesService;

        public DeleteRecipeRequestHandler(IRecipesService recipesService)
        {
            _recipesService = recipesService;
        }

        public async Task<Response<RecipeDTO>> Handle(
            DeleteRecipeRequest request, CancellationToken cancellationToken)
            => await _recipesService.DeleteAsync(request.Id);
    }
}
