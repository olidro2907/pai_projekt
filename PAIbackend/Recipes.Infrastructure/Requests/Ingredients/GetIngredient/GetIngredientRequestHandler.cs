﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Ingredients.GetIngredient
{
    class GetIngredientRequestHandler : IRequestHandler<GetIngredientRequest, Response<IngredientDTO>>
    {
        private readonly IIngredientsService _ingredientsService;

        public GetIngredientRequestHandler(IIngredientsService ingredientsService)
        {
            _ingredientsService = ingredientsService;
        }

        public async Task<Response<IngredientDTO>> Handle(
            GetIngredientRequest request, CancellationToken cancellationToken)
            => await _ingredientsService.GetByIdAsync(request.Id);
    }
}
