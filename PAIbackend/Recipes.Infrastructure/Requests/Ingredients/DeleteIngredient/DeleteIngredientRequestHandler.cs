﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Ingredients.DeleteIngredient
{
    public class DeleteIngredientRequestHandler : IRequestHandler<DeleteIngredientRequest, Response<IngredientDTO>>
    {
        private readonly IIngredientsService _ingredientsService;

        public DeleteIngredientRequestHandler(IIngredientsService ingredientsService)
        {
            _ingredientsService = ingredientsService;
        }

        public async Task<Response<IngredientDTO>> Handle(
            DeleteIngredientRequest request, CancellationToken cancellationToken)
            => await _ingredientsService.DeleteAsync(request.Id);
    }
}
