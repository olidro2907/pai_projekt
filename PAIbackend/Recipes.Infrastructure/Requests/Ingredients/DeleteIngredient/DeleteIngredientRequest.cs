﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Ingredients.DeleteIngredient
{
    public class DeleteIngredientRequest : IRequest<Response<IngredientDTO>>
    {
        public DeleteIngredientRequest(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
