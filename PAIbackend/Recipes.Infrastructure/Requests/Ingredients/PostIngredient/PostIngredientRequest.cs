﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Ingredients.PostIngredient
{
    public class PostIngredientRequest : IRequest<Response<IngredientDTO>>
    {
        public string Name { get; set; }
        public string Amount { get; set; }
        public Guid RecipeId { get; set; }

    }
}
