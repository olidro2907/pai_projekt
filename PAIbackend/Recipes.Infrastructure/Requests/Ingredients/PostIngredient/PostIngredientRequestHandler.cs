﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Ingredients.PostIngredient
{
    public class PostIngredientRequestHandler : IRequestHandler<PostIngredientRequest, Response<IngredientDTO>>
    {
        private readonly IIngredientsService _ingredientsService;

        public PostIngredientRequestHandler(IIngredientsService ingredientsService)
        {
            _ingredientsService = ingredientsService;
        }

        public async Task<Response<IngredientDTO>> Handle(
            PostIngredientRequest request, CancellationToken cancellationToken)
            => await _ingredientsService.PostAsync(request);
    }
}
