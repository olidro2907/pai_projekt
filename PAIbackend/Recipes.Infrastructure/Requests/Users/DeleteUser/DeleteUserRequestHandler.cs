﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Users.DeleteUser
{
    class DeleteUserRequestHandler : IRequestHandler<DeleteUserRequest, Response<UserDTO>>
    {
        private readonly IUsersService _usersService;
        public DeleteUserRequestHandler(IUsersService usersService)
        {
            _usersService = usersService;
        }
        public async Task<Response<UserDTO>> Handle(
           DeleteUserRequest request, CancellationToken cancellationToken)
           => await _usersService.DeleteAsync(request.Id);

    }
}
