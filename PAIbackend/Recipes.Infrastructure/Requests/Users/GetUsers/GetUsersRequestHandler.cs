﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Users.GetUsers
{
    public class GetUsersRequestHandler : IRequestHandler<GetUsersRequest, Response<IEnumerable<UserDTO>>>
    {
        private readonly IUsersService _usersService;

        public GetUsersRequestHandler(IUsersService usersService)
        {
            _usersService = usersService;
        }

        public async Task<Response<IEnumerable<UserDTO>>> Handle(
            GetUsersRequest request, CancellationToken cancellationToken)
            => await _usersService.GetUsersAsync();
    }
}
