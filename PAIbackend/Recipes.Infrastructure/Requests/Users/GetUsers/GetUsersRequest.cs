﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Users.GetUsers
{
    public class GetUsersRequest : IRequest<Response<IEnumerable<UserDTO>>>
    {
    }
}
