﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Users.UpdateUser
{
    class UpdateUserRequestHandler : IRequestHandler<UpdateUserRequest, Response<UserDTO>>
    {
        private readonly IUsersService _usersService;
        public UpdateUserRequestHandler(IUsersService usersService)
        {
            _usersService = usersService;
        }
        public async Task<Response<UserDTO>> Handle(
           UpdateUserRequest request, CancellationToken cancellationToken)
           => await _usersService.UpdateAsync(request);

    }
}
