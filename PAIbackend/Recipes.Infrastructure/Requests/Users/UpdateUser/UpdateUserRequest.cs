﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using System;

namespace Recipes.Infrastructure.Requests.Users.UpdateUser
{
    public class UpdateUserRequest : IRequest<Response<UserDTO>>
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }
    }
}
