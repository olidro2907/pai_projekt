﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;

namespace Recipes.Infrastructure.Requests.Identity.ChangePassword
{
    public class ChangePasswordRequest : IRequest<Response<StatusResponseDTO>>
    {
        public string Password { get; set; }

        public string NewPassword { get; set; }
    }
}
