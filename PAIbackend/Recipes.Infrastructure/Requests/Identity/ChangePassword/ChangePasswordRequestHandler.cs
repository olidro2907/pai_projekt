﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Identity.ChangePassword
{
    class ChangePasswordRequestHandler : IRequestHandler<ChangePasswordRequest, Response<StatusResponseDTO>>
    {
        private readonly IIdentityService _identityService;

        public ChangePasswordRequestHandler(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public async Task<Response<StatusResponseDTO>> Handle(
            ChangePasswordRequest request, CancellationToken cancellationToken)
            => await _identityService.ChangePasswordAsync(
                request.Password, request.NewPassword);
    }
}
