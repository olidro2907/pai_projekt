﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Identity.RefreshToken
{
    public class RefreshTokenRequestHandler :
        IRequestHandler<RefreshTokenRequest, Response<AuthResponseDTO>>
    {
        private readonly IJwtTokenService _jwtTokenService;

        public RefreshTokenRequestHandler(IJwtTokenService jwtTokenService)
        {
            _jwtTokenService = jwtTokenService;
        }

        public async Task<Response<AuthResponseDTO>> Handle(
            RefreshTokenRequest request, CancellationToken cancellationToken)
            => await _jwtTokenService.RefreshTokenAsync(
                request.Token, request.RefreshToken);
    }
}
