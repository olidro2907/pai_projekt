﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;

namespace Recipes.Infrastructure.Requests.Identity.RefreshToken
{
    public class RefreshTokenRequest : IRequest<Response<AuthResponseDTO>>
    {
        public string Token { get; set; }

        public string RefreshToken { get; set; }
    }
}
