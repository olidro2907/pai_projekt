﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Identity.SignIn
{
    public class SignInRequestHandler :
        IRequestHandler<SignInRequest, Response<AuthResponseDTO>>
    {
        private readonly IIdentityService _identityService;

        public SignInRequestHandler(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public async Task<Response<AuthResponseDTO>> Handle(SignInRequest request,
            CancellationToken cancellationToken)
            => await _identityService.SignInAsync(request.Email, request.Password);
    }
}
