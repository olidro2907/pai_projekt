﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Identity.SignIn
{
    public class SignInRequest : IRequest<Response<AuthResponseDTO>>
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
