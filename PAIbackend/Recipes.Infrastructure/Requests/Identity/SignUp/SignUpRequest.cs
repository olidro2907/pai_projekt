﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;

namespace Recipes.Infrastructure.Requests.Identity.SignUp
{
    public class SignUpRequest : IRequest<Response<StatusResponseDTO>>
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}
