﻿using MediatR;
using Recipes.Infrastructure.Common;
using Recipes.Infrastructure.DTO;
using Recipes.Infrastructure.Services.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace Recipes.Infrastructure.Requests.Identity.SignUp
{
    public class SignUpRequestHandler :
        IRequestHandler<SignUpRequest, Response<StatusResponseDTO>>
    {
        private readonly IIdentityService _identityService;

        public SignUpRequestHandler(IIdentityService identityService)
        {
            _identityService = identityService;
        }

        public async Task<Response<StatusResponseDTO>> Handle(SignUpRequest request,
            CancellationToken cancellationToken)
            => await _identityService.SignUpAsync(
                request.Name, request.Surname, request.Email, request.Password);
    }
}
