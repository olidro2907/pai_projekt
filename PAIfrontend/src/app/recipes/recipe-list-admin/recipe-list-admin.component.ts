import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Recipe } from "src/app/models/recipe";
import { RecipeService } from "src/app/shared/recipe.service";

@Component({
  selector: 'app-recipe-list-admin',
  templateUrl: './recipe-list-admin.component.html',
  styleUrls: ['./recipe-list-admin.component.css']
})
export class RecipeListAdminComponent implements OnInit {
  recipes: Recipe[];
  noRecipes: Recipe = { description: "Żaden użytkownik nie dodał jeszcze przepisu", name: "Brak przepisów", id: '0', type: "", ingredients: []};
  loading: boolean = true;

  constructor(
    public dialog: MatDialog,
    private recipeService: RecipeService) { }

    ngOnInit(): void {
      this.recipeService.getRecipes().subscribe(res => {
        this.recipes = res.data.length != 0 ? res.data : [];
        this.loading = false;
      });
    }

}
