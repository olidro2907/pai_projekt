import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RecipeService } from 'src/app/shared/recipe.service';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-edit-recipe',
  templateUrl: './edit-recipe.component.html',
  styleUrls: ['./edit-recipe.component.css']
})
export class EditRecipeComponent implements OnInit {
  title: string;
  description: string;
  recipeId: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, 
  public dialogRef: MatDialogRef<EditRecipeComponent>, 
  private recipeService: RecipeService, 
  private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.title = this.data.recipe.name;
    this.description = this.data.recipe.description;
    this.recipeId = this.data.recipe.id;
  }

  onEditClick() {
    var details = { id:this.recipeId, name: this.title.trim().replace(/  +/g, ' '), description: this.description.trim().replace(/  +/g, ' ') };
    this.recipeService.updateRecipeDetails(details).subscribe((res) => {
      this.data.recipe.description = details.description;
      this.data.recipe.name = details.name;
      this.toastr.success("Pomyślnie zaktualizowano przepis", "Pomyślnie zaktualizowano");
      this.dialogRef.close();
    },
      (err) => {
        this.toastr.error("Nie można zmienić nazwy", "Wystąpił błąd");
        this.title = this.data.recipe.name;
        this.description = this.data.recipe.description;
      });
  }

  onKeyPress(event: any, element: any) {
    if (element.length == 0 && event.keyCode == 32)
      return false;

    return true;
  };

  onCloseClick() {
    this.dialogRef.close();
  }

}
