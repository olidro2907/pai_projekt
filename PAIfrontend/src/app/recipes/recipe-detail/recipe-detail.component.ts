import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Ingredient } from 'src/app/models/ingredient';
import { Recipe } from 'src/app/models/recipe';
//import { EditChoiceComponent } from 'src/app/questions/edit-choice/edit-choice.component';
import { EditIngredientComponent } from 'src/app/ingredients/edit-ingredient/edit-ingredient.component';
import { NewIngredientComponent } from 'src/app/ingredients/new-ingredient/new-ingredient.component';
import { ConfirmationDialogComponent } from 'src/app/shared/dialogs/confirmation-dialog/confirmation-dialog.component';
import { IngredientService } from 'src/app/shared/ingredient.service';
import { RecipeService } from 'src/app/shared/recipe.service';
import { EditRecipeComponent } from '../edit-recipe/edit-recipe.component';
import { ToastrService } from "ngx-toastr";


@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipe: Recipe;
  routeSubscription: Subscription;
  recipeId: number;
  loading: boolean = true;
  loadingFile: boolean = false;

  constructor(
    private recipeService: RecipeService,
    private ingredientService: IngredientService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit(): void {
    this.routeSubscription = this.route.params.subscribe(params => {
      this.recipeId = params["id"];
      this.recipeService.getRecipe(this.recipeId).subscribe(res => {
        this.recipe = res.data;
        this.recipe.ingredients.forEach(x => x.id);
        this.loading = false;
      });
    });
  }

  openAddIngredientDialog() {
    const dialogRef = this.dialog.open(NewIngredientComponent, {
      width: "800px",
      maxHeight: "800px",
      data: { recipe: this.recipe }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.recipe.ingredients.push(result.data);
      }
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  onDeleteRecipeClick() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      height: "160px",
      width: "400px",
      data: { "dialogText": "Czy na pewno chcesz usunąć tą ankietę?" }
    });

    dialogRef.afterClosed().subscribe((deleteRecipe: boolean) => {
      if (deleteRecipe) {
        this.recipeService.deleteRecipe(this.recipeId).subscribe(res => {
          this.toastr.success("Pomyślnie usunięto przepis.", "Usuwanie przepisu");

          this.router.navigate(['admin/recipes']);
        },
      )
      }
    });
  }

  onDeleteIngredientClick(ingredient: Ingredient) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      height: "160px",
      width: "400px",
      data: { "dialogText": "Czy na pewno chcesz usunąć ten składnik?" }
    });

    dialogRef.afterClosed().subscribe((deleteIngredient: boolean) => {
      if (deleteIngredient) {
        this.ingredientService.deleteIngredient(ingredient.id).subscribe((res) => {
          this.recipe.ingredients.splice(this.recipe.ingredients.indexOf(ingredient), 1);
          this.toastr.success("Pomyślnie usunięto składnik przepisu.", "Usunięto składnik");
        },
        );
      }
    });
  }

  onEditRecipeClick() {
    const dialogRef = this.dialog.open(EditRecipeComponent, {
      width: "800px",
      maxHeight: "650px",
      data: { recipe: this.recipe }
    });
  }

  onEditIngredientClick(ingredient: any) {
    const dialogRef = this.dialog.open(EditIngredientComponent, {
      width: "800px",
      maxHeight: "400px",
      data: { recipe: this.recipe, ingredient: ingredient }
    });
  }

}
