import { ToastrService } from "ngx-toastr";
import { Recipe } from "src/app/models/recipe";
import { Component, OnInit, Inject } from '@angular/core';
import { RecipeService } from 'src/app/shared/recipe.service';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: 'app-new-recipe',
  templateUrl: './new-recipe.component.html',
  styleUrls: ['./new-recipe.component.css']
})
export class NewRecipeComponent implements OnInit {
  recipe: Recipe;
  selectedOption = "Dish";
  recipeTypes = [
    { value: "Zoup", name: "Zupa" },
    { value: "Dip", name: "Przystawka" },
    { value: "Dish", name: "Danie główne" },
    { value: "Deser", name: "Deser" },

  ];
  constructor(public dialogRef: MatDialogRef<NewRecipeComponent>,
    private recipeService: RecipeService,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.recipe = new Recipe();
    this.recipe.name = "";
    this.recipe.description = "";
  }

  onSubmit() {
    this.recipe.name = this.recipe.name.trim().replace(/  +/g, ' ');
    this.recipe.description = this.recipe.description.trim().replace(/  +/g, ' ');

    this.recipeService.postRecipe(this.recipe).subscribe(res => {
      this.recipe.id = res.data.id;
      this.data.recipes.push(this.recipe);
      this.toastr.success("Pomyślnie dodano przepis", "Dodano przepis");
      this.dialogRef.close();
    }, (err) => {
      this.toastr.error("Spróbuj ponownie.", "Błąd podczas dodawania przepisu");
    });
  }
  
  onKeyPress(event: any, element: any) {
    if (element.length == 0 && event.keyCode == 32)
      return false;

    return true;
  };

  onCloseClick() {
    this.dialogRef.close();
  }

}
