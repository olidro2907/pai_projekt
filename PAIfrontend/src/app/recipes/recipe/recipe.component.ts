import { Component, Input, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Recipe } from 'src/app/models/recipe';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit {

  @Input() recipe: Recipe;

  constructor(private router: Router) { }

  ngOnInit() { }

  onDetailsClick() {
    if (this.recipe.id != '0')
      this.router.navigate(["user/recipe", this.recipe.id]);
  }

}
