import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Recipe } from "src/app/models/recipe";
import { RecipeService } from "src/app/shared/recipe.service";
import { NewRecipeComponent } from '../new-recipe/new-recipe.component';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[];
  noRecipes: Recipe = { description: "Naciśnij przycisk 'Dodaj przepis', aby dodać nowy przepis", name: "Brak przepisów", id: '0', type: "", ingredients: []};
  loading: boolean = true;
  
  constructor(    
  public dialog: MatDialog,
  private recipeService: RecipeService) { }

  ngOnInit(): void {
    this.recipeService.getRecipes().subscribe(res => {
      this.recipes = res.data.length != 0 ? res.data : [];
      this.loading = false;
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(NewRecipeComponent, {
      height: "600px",
      width: "600px",
      data: { recipes: this.recipes }
    });
  }

}
