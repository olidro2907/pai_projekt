import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Ingredient } from "../models/ingredient";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: "root"
})
export class IngredientService {
  private readonly BaseURI: string;

  constructor(private http: HttpClient) {
    this.BaseURI = environment.apiUrl + '/ingredients';
  }

  postIngredientRecipe(ingredient: Ingredient) {
    return this.http.post<Ingredient>(
      this.BaseURI,
      ingredient
    );
  }

  deleteIngredient(id: string) {
    return this.http.delete(this.BaseURI + "/" + id);
  }

  updateIngredient(question: Ingredient) {
    return this.http.put(this.BaseURI, question);
  }
}
