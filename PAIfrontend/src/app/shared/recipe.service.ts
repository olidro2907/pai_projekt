import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Recipe } from "../models/recipe";
import { Observable } from "rxjs";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: "root"
})
export class RecipeService {
  readonly BaseURI: string;

  constructor(private http: HttpClient) {
    this.BaseURI = environment.apiUrl + '/';
  }

  postRecipe(recipe: Recipe): Observable<any> {
    return this.http.post<Recipe>(this.BaseURI + "recipes", recipe);
  }

  getRecipes(): Observable<any> {
    return this.http.get(this.BaseURI + "recipes");
  }

  getRecipe(id: number): Observable<any> {
    return this.http.get(this.BaseURI + "recipes/" + id);
  }

  deleteRecipe(id: number) {
    return this.http.delete(this.BaseURI + "recipes/" + id);
  }

  updateRecipeDetails(details: any) {
    return this.http.put(this.BaseURI + "recipes/", details);
  }


}
