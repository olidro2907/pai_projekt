import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { RecipeListComponent } from './recipes/recipe-list/recipe-list.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { UserComponent } from './user/dashboard/user/user.component';
import { LoginComponent } from "./user/login/login.component";
import { RegistrationComponent } from "./user/registration/registration.component";
import {AdminComponent} from "./user/dashboard/admin/admin.component";
import {AuthGuard} from "./auth/auth.guard";
import {RoleGuard} from "./auth/role.guard";
import {Role} from "./models/role";
import { UsersListComponent } from './user/users-list/users-list.component';
import { RecipeListAdminComponent } from './recipes/recipe-list-admin/recipe-list-admin.component';


const routes: Routes = [
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "admin",
    component: AdminComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: { roles: [Role.Admin] },
    children: [
      {
        path: "",
        redirectTo: "recipes",
        pathMatch: "full",
      },
      {
        path: "recipes",
        component: RecipeListAdminComponent,
      },
      {
        path: "registration",
        component: RegistrationComponent,
      },
      {
        path: "users",
        component: UsersListComponent,
      },
    ]
  },
  {
    path: "user",
    component: UserComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: { roles: [Role.User] },
    children: [
      {
        path: "",
        redirectTo: "recipes",
        pathMatch: "full",
        data: { roles: [Role.User] }
      },
      {
        path: "recipes",
        component: RecipeListComponent,
        data: { roles: [Role.User] }
      },
      {
        path: "recipe/:id",
        component: RecipeDetailComponent,
      },
    ]
  },
  {
    path: '**',
    redirectTo: "login"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
