import { Recipe } from "./recipe";

export interface DialogData {
  recipe: Recipe;
}
