import { Ingredient } from "./ingredient";

export class Recipe {

  constructor() {
    this.ingredients = new Array<Ingredient>();
  }

  id: string;
  name: string;
  description: string;
  //imageUrl: string;
  type: string;
  ingredients: Ingredient[];
}