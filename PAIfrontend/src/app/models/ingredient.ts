export class Ingredient {
  id: string;
  recipeId: string;
  name: string;
  amount: string;
}