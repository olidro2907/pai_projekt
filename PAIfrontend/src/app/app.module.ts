import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ToastrModule } from "ngx-toastr";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { UserService } from "./shared/user.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LoginComponent } from "./user/login/login.component";
import { AuthInterceptor } from "./auth/auth.interceptor";
import { MaterialModule } from "./material/material.module";
import { AuthService } from "./shared/auth.service";
import { AppComponent } from './app.component';
import { AdminComponent } from "./user/dashboard/admin/admin.component";
import { RecipeListComponent } from './recipes/recipe-list/recipe-list.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { UsersListComponent } from './user/users-list/users-list.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { ConfirmationDialogComponent } from './shared/dialogs/confirmation-dialog/confirmation-dialog.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { ChangePasswordComponent } from './user/change-password/change-password.component';
import { UserComponent } from './user/dashboard/user/user.component';
import { NewRecipeComponent } from './recipes/new-recipe/new-recipe.component';
import { EditRecipeComponent } from './recipes/edit-recipe/edit-recipe.component';
import { RecipeComponent } from './recipes/recipe/recipe.component';
import { NewIngredientComponent } from './ingredients/new-ingredient/new-ingredient.component';
import { EditIngredientComponent } from './ingredients/edit-ingredient/edit-ingredient.component';
import { RecipeListAdminComponent } from './recipes/recipe-list-admin/recipe-list-admin.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    LoginComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    UsersListComponent,
    EditUserComponent,
    ConfirmationDialogComponent,
    SidebarComponent,
    NavbarComponent,
    RegistrationComponent,
    ChangePasswordComponent,
    UserComponent,
    NewRecipeComponent,
    EditRecipeComponent,
    RecipeComponent,
    NewIngredientComponent,
    EditIngredientComponent,
    RecipeListAdminComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    AuthService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
  ]
})
export class AppModule { }
