import { Component, Input, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from 'src/app/models/dialogData';
import { IngredientService } from 'src/app/shared/ingredient.service';
import { Ingredient } from "src/app/models/ingredient";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-new-ingredient',
  templateUrl: './new-ingredient.component.html',
  styleUrls: ['./new-ingredient.component.css']
})
export class NewIngredientComponent implements OnInit {
  ingredient: Ingredient;

  constructor(
    public dialogRef: MatDialogRef<NewIngredientComponent>,
    private ingredientService: IngredientService,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
    this.ingredient = new Ingredient();
    this.ingredient.name = "";
    this.ingredient.amount = "";
  }

  onSubmit() {
    this.ingredient.name = this.ingredient.name.trim().replace(/  +/g, ' ');
    this.ingredient.amount = this.ingredient.amount.trim().replace(/  +/g, ' ');
    this.ingredient.recipeId = this.data.recipe.id

    this.ingredientService.postIngredientRecipe(this.ingredient).subscribe(res => {
      this.toastr.success("Pomyślnie dodano składnik", "Dodano składnik");
      this.dialogRef.close();
    }, (err) => {
      this.toastr.error("Spróbuj ponownie.", "Błąd podczas dodawania składnika");
    });
  }

  onKeyPress(event: any, element: any) {
    if (element.length == 0 && event.keyCode == 32)
      return false;

    return true;
  };

  onCloseClick() {
    this.dialogRef.close();
  }

}
