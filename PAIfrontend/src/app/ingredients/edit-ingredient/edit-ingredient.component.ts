import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IngredientService } from 'src/app/shared/ingredient.service';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-edit-ingredient',
  templateUrl: './edit-ingredient.component.html',
  styleUrls: ['./edit-ingredient.component.css']
})
export class EditIngredientComponent implements OnInit {
  name: string;
  amount: string;
  recipeId: string;
  ingredient: any;

  constructor(
  @Inject(MAT_DIALOG_DATA) public data: any, 
  public dialogRef: MatDialogRef<EditIngredientComponent>, 
  private ingredientService: IngredientService, 
  private toastr: ToastrService) { }

  ngOnInit(): void {
    this.name = this.data.ingredient.name;
    this.amount = this.data.ingredient.amount;
    this.recipeId = this.data.ingredient.recipeId;
    this.ingredient = this.data.ingredient;
  }

  onEditClick() {
    var ingredient = { id:this.ingredient.id, name: this.name.trim().replace(/  +/g, ' '), amount: this.amount.trim().replace(/  +/g, ' '), recipeId: this.recipeId };
    this.ingredientService.updateIngredient(ingredient).subscribe((res) => {
      this.data.ingredient.amount = ingredient.amount;
      this.data.ingredient.name = ingredient.name;
      this.toastr.success("Pomyślnie zaktualizowano składnik", "Pomyślnie zaktualizowano");
      this.dialogRef.close();
    },
      (err) => {
        this.toastr.error("Nie można zmienić nazwy", "Wystąpił błąd");
        this.name = this.data.ingredient.name;
        this.amount = this.data.ingredient.description;
      });
  }

  onKeyPress(event: any, element: any) {
    if (element.length == 0 && event.keyCode == 32)
      return false;

    return true;
  };

  onCloseClick() {
    this.dialogRef.close();
  }

}
